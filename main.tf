resource "proxmox_vm_qemu" "proxmox_vm" {
  count             = length(var.ips)
  name              = "${var.name_prefix}-${count.index}"
  target_node       = var.target_node
  clone             = var.template_name
  full_clone        = true
  os_type           = "cloud-init"
  cores             = var.flavors[index(var.flavors.*.name, var.flavor_name)].cpu
  sockets           = "1"
  cpu               = "host"
  memory            = var.flavors[index(var.flavors.*.name, var.flavor_name)].memory
  scsihw            = "virtio-scsi-pci"
  bootdisk          = "scsi0"
  agent             = 1
  disk {
    id              = 0
    size            = var.flavors[index(var.flavors.*.name, var.flavor_name)].storage
    type            = "scsi"
    storage         = var.storage_pool
    storage_type    = var.storage_type
    iothread        = true
  }
  network {
    id              = 0
    model           = "virtio"
    bridge          = var.bridge
  }
  # Cloud Init Settings
  ipconfig0 = "ip=${var.ips[count.index]}/24,gw=${var.gateway}"
  ciuser = "ansible"
  sshkeys = var.sshkeys
}
