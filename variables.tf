variable "ips" {
  description = "List of IPs for cluster nodes"
  type        = list(string)
}

variable "storage_type" {
  description = "Specify storage type"
  type = string
  default = "lvm"
}

variable "name_prefix" {
  description = "Prefix for node names"
  type = string
}

variable "cores" {
  description = "number of cores to give each vm"
  type = number
  default = 2
}

variable "memory" {
  description = "amount of memory in MB give each vm"
  type = number
  default = 2048
}

variable "sshkeys" {
  description = "ssh keys to drop onto each vm"
  type = string
}

variable "ssh_user" {
  description = "user to put ssh keys under"
  type = string
  default = "ansible"
}

variable "gateway" {
  description = "gateway for cluster"
  type = string
}

variable "bridge" {
  description = "bridge to use for network"
  type = string
  default = "vmbr0"
}

variable "storage_pool" {
  description = "storage pool to use for disk"
  type = string
  default = "local-lvm"
}

variable "target_node" {
  description = "node to deploy on"
  type = string
}

variable "template_name" {
  description = "template to use"
  type = string
  default = "centos-7-template"
}

variable "flavor_name" {
  description = "Flavor name"
  type = string
  default = "t3.nano"
}

variable "flavors" {
  description = "List of flavor"
  type = list(object({
    name = string,
    cpu = number,
    memory = number,
    storage = number
  }))
  default = [
    { "name" = "t3.nano", "cpu" = 2, "memory" = 512, "storage" = 10 },
    { "name" = "t3.micro", "cpu" = 2, "memory" = 1024, "storage" = 10 },
    { "name" = "t3.small", "cpu" = 2, "memory" = 2048, "storage" = 20 },
    { "name" = "t3.medium", "cpu" = 2, "memory" = 4096, "storage" = 30 },
    { "name" = "t3.large", "cpu" = 2, "memory" = 8192, "storage" = 50 },
    { "name" = "t3.xlarge", "cpu" = 4, "memory" = 16384, "storage" = 50 },
    { "name" = "t3.2xlarge", "cpu" = 8, "memory" = 32768, "storage" = 80 },
    { "name" = "i3.nano", "cpu" = 2, "memory" = 512, "storage" = 50 },
    { "name" = "i3.micro", "cpu" = 2, "memory" = 1024, "storage" = 70 },
    { "name" = "i3.small", "cpu" = 2, "memory" = 2048, "storage" = 70 },
    { "name" = "i3.medium", "cpu" = 2, "memory" = 2048, "storage" = 100 },
    { "name" = "i3.large", "cpu" = 2, "memory" = 4096, "storage" = 100 },
    { "name" = "i3.xlarge", "cpu" = 4, "memory" = 4096, "storage" = 200}
  ]
}